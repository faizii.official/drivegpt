// Define the HC-05 module's RX and TX pins
const int hc05RxPin = 13;    // Connect HC-05 TX to ESP32 pin D2
const int hc05TxPin = 12;    // Connect HC-05 RX to ESP32 pin D3

HardwareSerial hc05Serial(1); // Use hardware serial port 1 (UART1)

void setup() {
  pinMode(2, OUTPUT); // Set pin as OUTPUT
  Serial.begin(115200);       // Initialize the hardware serial port
  
  hc05Serial.begin(9600, SERIAL_8N1, hc05RxPin, hc05TxPin); // Initialize HC-05 serial
  
  Serial.println("ESP32 and HC-05 Serial Communication Example");
      Serial.print("started");
  pinMode(21, OUTPUT); // Set pin as OUTPUT
  delay(1000);
  // digitalWrite(21, HIGH);
  

}

void loop() {
  // Read data from HC-05 and send it to the serial monitor
  while (hc05Serial.available()) {
    char c = hc05Serial.read();
    Serial.print(c);
    digitalWrite(2, HIGH);
    delay(1);
    digitalWrite(2, LOW);

  }
  
  // Read data from serial monitor and send it to HC-05
  while (Serial.available()) {
    char c = Serial.read();
    hc05Serial.print(c);
    digitalWrite(2, HIGH);
    delay(1);
    digitalWrite(2, LOW);
    
  }
}
